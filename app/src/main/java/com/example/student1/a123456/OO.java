package com.example.student1.a123456;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by student1 on 21.11.16.
 */
public class OO extends MainActivity {

    TextView tGreetings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Intent intent = getIntent();
        String username = intent.getExtras().getString("login");


        tGreetings.setText("Hello, " + username);

    }
}
